set -x
stat --format "%N %s %f %Y" branches/fuse

# error checks
stat branches/not_existing
stat branches/not_existing/1
