set -x
stat --format '%N %s %f %Y' commits/08370e5f34b630186937103871031310953ac1bb
stat --format '%N %s %f %Y' commits/08370e5f34b630186937103871031310953ac1bb/files
head -n 2 commits/08370e5f34b630186937103871031310953ac1bb/files/Copying
stat --format '%N %s %f %Y' commits/08370e5f34b630186937103871031310953ac1bb/files/src/*
stat --format '%N %s %f %Y' commits/08370e5f34b630186937103871031310953ac1bb/files/src/gitpp/*

# error checks
stat commits/08370e5f34b630186937103871031310953ac1bb/files/src/not_existing
stat commits/08370e5f34b630186937103871031310953ac1bb/files/src/not_existing/1
