set -x
stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/parent   
stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/parents
stat --format '%N %s %f %Y' commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/parents/*

# error checks
stat commits/dc0e339816b751ba3a404e5301c4646a3438f3bd/parents/not_existing
