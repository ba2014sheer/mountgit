set -x
stat --format '%N %f' *

# error checks
stat not_existing
stat not_existing/1
