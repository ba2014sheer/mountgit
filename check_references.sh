#!/bin/zsh

REFERENCES_REPOSITORY="."
SHELL_FOR_REFERENCES="/bin/zsh"
print_diff=false

CXXFLAGS=-Wall -Werror -Wno-parentheses -pipe -std=c++17 -O0 -ggdb -fprofile-arcs -ftest-coverage 

while [ $# -gt 0 ]; do
  if [ "$1" = "--print-diff" ]; then
    print_diff=true
    shift
  elif [ -d "$1" ]; then
    if [ -d "$1/.git" -o -f "$1/HEAD" ]; then
      REFERENCES_REPOSITORY="$1"
      shift
    else
      echo "$1 is no git directory" >&2
      exit -1
    fi
  else
    echo "Wrong usage: ./check_references [--print-diff] [repository]" >&2
    exit -1
  fi
done

rm mountgit
make mountgit
test -f mountgit || exit 1
count_ref=0
count_ref_good=0
count_ref_failed=0
PS4="$ "
test -d mnt || mkdir mnt

find references -name "*.sh" \
  | while read reference; do
  echo "checking reference ${reference}..."
  count_ref=$((${count_ref} + 1))
  reference_output_ref="$(dirname $reference)/$(basename $reference .sh)".ref
  reference_output="$(dirname $reference)/$(basename $reference .sh)".out
  ./mountgit "${REFERENCES_REPOSITORY}" mnt
  cd mnt/
  LANG=C PS4="$ " "${SHELL_FOR_REFERENCES}" "../$reference" >"../${reference_output}" 2>&1
  cd - >/dev/null
  fusermount -u -z mnt
  if [ ! -f "${reference_output_ref}" ]; then
    count_ref_failed=$((${count_ref_failed} + 1))
    echo -n -e "\e[01;31m"
    echo "...ref missing"
    echo -n -e "\e[;0m"
  elif cmp --silent <(< "${reference_output}" tr -d "'") <(< "${reference_output_ref}" tr -d "'"); then
    count_ref_good=$((${count_ref_good} + 1))
    echo -n -e "\e[01;32m"
    echo "...good"
    echo -n -e "\e[;0m"
  else
    #diff ${reference_output} ${reference_output_ref}
    if [ "${print_diff}" = "true" ]; then
      diff <(< "${reference_output}" tr -d "'") <(< "${reference_output_ref}" tr -d "'");
    fi
    count_ref_failed=$((${count_ref_failed} + 1))
    echo -n -e "\e[01;31m"
    echo "...failed"
    echo -n -e "\e[;0m"
  fi
done

echo
echo -n -e "\e[01;1m"
echo "Summary"
echo -n -e "\e[;0m"
echo "total:  ${count_ref}"
if [ ${count_ref_failed} -eq 0 ]; then
  echo -n -e "\e[01;32m"
  echo "good:   ${count_ref_good}"
  echo -n -e "\e[;0m"
  echo "failed: ${count_ref_failed}"
else
  echo "good:   ${count_ref_good}"
  echo -n -e "\e[01;31m"
  echo "failed: ${count_ref_failed}"
fi
echo -n -e "\e[;0m"

gcovr --print-summary src | grep "^TOTAL" | sed -E "s/^TOTAL +([0-9]+) +([0-9]+) +([0-9]+%)$/Coverage: \2\/\1 = \3/"

exit ${count_ref_failed}
