#pragma once

#include "gitfuse.h"

class GitFuse::Tags {
  public:
    explicit Tags(Git::Repository& repository);

    auto repository() -> Git::Repository&;

    auto getattr(Path path)      -> FileStatus;
    auto readdir(Directory path) -> vector<Path>;
    auto readlink(Path path)     -> Path;
    auto read(File path)         -> string;
  private:
    Git::Repository& repository_;
};
