#include "../constants.h"

#include "commit.h"

GitFuse::Commit::Commit(Git::Commit const& commit) :
  commit_(commit),
  tree_(commit_)
{ }

GitFuse::Commit::Commit(Git::Commit&& commit) :
  commit_(std::move(commit)),
  tree_(commit_)
{ }

auto GitFuse::Commit::commit()
  -> Git::Commit&
{ return commit_; }

auto GitFuse::Commit::getattr(Path const path)
  -> FileStatus
{
  if (path.empty()) {
    FileStatus status;
    status.atime = commit_.time();
    status.mtime = commit_.time();
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = readdir("").size();
    return status;
  }

  auto const subdir = first_directory(path);
  auto const remaining_path = without_first_directory(path);

  if (remaining_path.empty()) {
    FileStatus status;
    status.atime = commit_.time();
    status.mtime = commit_.time();

    if (   path == "id"
        || path == "message"
        || path == "summary"
        || path == "body"
        || path == "time"
        || path == "committer"
        || path == "author") {
      status.type = file_type::regular;
      status.mode = 0444;
      status.size = read(path).size();
      return status;
    } else if (path == "parents") {
      status.type = file_type::directory;
      status.mode = 0555;
      status.size = readdir(path).size();
      return status;
    } else if (   path == "parent"
               && commit_.parents().size() == 1) {
      status.type = file_type::symlink;
      status.mode = 0555;
      status.size = 40;
      return status;
    } else if (path == "children") {
      status.type = file_type::directory;
      status.mode = 0555;
      status.size = (options.with_childs_count ? readdir(path).size() : 1024);
      return status;
    } else if (   options.with_child_reference
               && path == "child"
               && commit_.children().size() == 1) {
      status.type = file_type::symlink;
      status.mode = 0555;
      status.size = 40;
      return status;
    } else if (path == "files") {
      auto status = tree_.getattr("");
      status.atime = commit_.time();
      status.mtime = commit_.time();
      return status;
    }
  } else if (subdir == "parents") {
    auto const parents = commit_.parents();
    auto commit = std::find_if(parents, [remaining_path](auto const& commit){
                               return commit.id() == remaining_path;});
    if (commit == parents.end())
      throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
    auto status = Commit(*commit).getattr("");
    status.type = file_type::symlink;
    status.mode = 0555;
    status.size = commit->id().size();
    return status;
  } else if (subdir == "children") {
    auto const children = commit_.children();
    auto commit = std::find_if(children, [remaining_path](auto const& commit){
                               return commit.id() == remaining_path;});
    if (commit == children.end())
      throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
    auto status = Commit(*commit).getattr("");
    status.type = file_type::symlink;
    status.mode = 0555;
    status.size = commit->id().size();
    return status;
  } else if (subdir == "files") {
    auto status = tree_.getattr(remaining_path);
    status.atime = commit_.time();
    status.mtime = commit_.time();
    return status;
  }

  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Commit::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "") {
    vector<Path> paths = {"id", "message", "summary", "body", "time", "committer", "author", "parents", "files"};
    if (commit_.parents().size() == 1)
      paths.emplace_back("parent");
    paths.emplace_back("children");
    if (options.with_child_reference) {
      if (commit_.children().size() == 1)
        paths.emplace_back("child");
    }
    return paths;
  }
  if (dir == "parents") {
    vector<Path> paths;
    for (auto const& parent : commit_.parents())
      paths.emplace_back(parent.id());
    return paths;
  }
  if (dir == "children") {
    vector<Path> paths;
    for (auto const& parent : commit_.children())
      paths.emplace_back(parent.id());
    return paths;
  }
  auto const subdir = first_directory(dir);
  auto const remaining_path = without_first_directory(dir);
  if (subdir == "files") {
    return tree_.readdir(remaining_path);
  }
  throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
}

auto GitFuse::Commit::readlink(Path const path)
  -> Path
{
  auto const subdir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (subdir == "files") {
    return tree_.readlink(remaining_path);
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Commit::read(File const path)
  -> string
{
  if (path == "id") {
    return commit_.id() + "\n";
  } else if (path == "message") {
    return commit_.message();
  } else if (path == "summary") {
    return commit_.summary() + "\n";
  } else if (path == "body") {
    return commit_.body() + "\n";
  } else if (path == "time") {
    return to_string(commit_.time()) + "\n";
  } else if (path == "committer") {
    return to_string(commit_.committer()) + "\n";
  } else if (path == "author") {
    return to_string(commit_.author()) + "\n";
  }
  auto const subdir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (subdir == "files") {
    return tree_.read(remaining_path);
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Commit::commit_id(Path const path)
  -> Git::Commit::Id
{
  if (path == "parent") {
    auto const parents = commit_.parents();
    if (parents.size() == 1)
      return parents[0].id();
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  if (   options.with_child_reference
      && path == "child") {
    auto const children = commit_.children();
    if (children.size() == 1)
      return children[0].id();
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  auto const subdir = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (subdir == "parents") {
    auto const parents = commit_.parents();
    if (std::count_if(parents, [remaining_path](auto const& commit) {
                      return commit.id() == remaining_path;})
        == 1) {
      auto commit = std::find_if(parents, [remaining_path](auto const& commit){
                                 return commit.id() == remaining_path;});
      return commit->id();
    }
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  if (subdir == "children") {
    auto const children = commit_.children();
    if (std::count_if(children, [remaining_path](auto const& commit) {
                      return commit.id() == remaining_path;})
        == 1) {
      auto commit = std::find_if(children, [remaining_path](auto const& commit){
                                 return commit.id() == remaining_path;});
      return commit->id();
    }
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
