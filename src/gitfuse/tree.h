#pragma once

#include "gitfuse.h"
#include "../gitpp/tree.h"

class GitFuse::Tree {
  public:
    explicit Tree(Git::Tree const& tree);
    explicit Tree(Git::Tree&& tree);

    auto tree() -> Git::Tree&;

    auto getattr(Path path)      -> FileStatus;
    auto readdir(Directory path) -> vector<Path>;
    auto readlink(Path path)     -> Path;
    auto read(File path)         -> string;
  private:
    Git::Tree tree_;
};
