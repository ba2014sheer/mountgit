#pragma once

#include "gitfuse.h"

#include "../gitpp/commits.h"

class GitFuse::Commits {
  public:
    explicit Commits(Git::Repository const& repository);

    auto commits()               -> Git::Commits const&;

    auto getattr(Path path)      -> FileStatus;
    auto readdir(Directory path) -> vector<Path>;
    auto readlink(Path path)     -> Path;
    auto read(File path)         -> string;
  private:
    Git::Commits commits_;
};
