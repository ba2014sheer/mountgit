#include "../constants.h"

#include "commits.h"
#include "commit.h"
#include "../gitpp/commits.h"

GitFuse::Commits::Commits(Git::Repository const& repository) :
  commits_(repository)
{ }

auto GitFuse::Commits::commits()
  -> Git::Commits const&
{ return commits_; }

auto GitFuse::Commits::getattr(Path const path)
  -> FileStatus
{
  if (path == "") {
    FileStatus status;
    status.atime = Time();
    status.mtime = Time();
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = readdir(path).size();
    return status;
  }

  auto const first_dir = first_directory(path);
  auto const remaining_path = without_first_directory(path);

  try {
    auto commit = commits_(first_dir);
    return Commit(std::move(commit)).getattr(remaining_path);
  } catch (git_error const& error) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
}

auto GitFuse::Commits::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "" || dir == ".") {
    vector<Path> entries;
    vector<Git::Commit> const& commits = commits_;
    for (auto const& commit : commits) {
      entries.push_back(commit.id());
    }
    return entries;
  }
  try {
    auto const commit_id = first_directory(dir);
    auto const remaining_path = without_first_directory(dir);
    return Commit(commits_(commit_id)).readdir(remaining_path);
  } catch (git_error const& error) {
    throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
  }
}

auto GitFuse::Commits::readlink(Path path)
  -> Path
{
  auto const commit_id = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (remaining_path.empty())
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  try {
    auto const subdir = first_directory(remaining_path);
    if (subdir == "files")
      return Commit(commits_(commit_id)).readlink(remaining_path);
    if (subdir == "parent" || subdir == "parents" || subdir == "child" || subdir == "children") {
      auto const id = Commit(commits_(commit_id)).commit_id(remaining_path);
      auto const base_path = (subdir == "parent" || subdir == "child" ? Path("..") : Path("../.."));
      return base_path/static_cast<string>(id);
    }
  } catch (git_error const& error) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Commits::read(File const path)
  -> string
{
  auto const commit_id = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (remaining_path.empty())
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  try {
    return Commit(commits_(commit_id)).read(remaining_path);
  } catch (git_error const& error) {
    throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
