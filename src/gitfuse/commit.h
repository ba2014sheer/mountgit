#pragma once

#include "gitfuse.h"
#include "tree.h"
#include "../gitpp/commit.h"

class GitFuse::Commit {
  public:
    explicit Commit(Git::Commit const& commit);
    explicit Commit(Git::Commit&& commit);

    auto commit() -> Git::Commit&;

    auto getattr(Path path)      -> FileStatus;
    auto readdir(Directory path) -> vector<Path>;
    auto readlink(Path path)     -> Path;
    auto read(File path)         -> string;
    auto commit_id(Path path)    -> Git::Commit::Id;
  private:
    Git::Commit commit_;
    Tree tree_;
};
