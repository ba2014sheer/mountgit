#include "../constants.h"

#include "branches.h"
#include "../gitpp/branch.h"
#include "../gitpp/commit.h"

GitFuse::Branches::Branches(Git::Repository& repository) :
  repository_(repository)
{ }

auto GitFuse::Branches::repository()
  -> Git::Repository&
{ return repository_; }

auto GitFuse::Branches::getattr(Path const path)
  -> FileStatus
{
  if (path == "" || path == "/") {
    FileStatus status;
    status.atime = Time();
    status.mtime = Time();
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = readdir(path).size();
    return status;
  }

  auto const first_dir = first_directory(path);
  auto const remaining_path = without_first_directory(path);

  try {
    auto branch = repository().branch(first_dir);

    if (remaining_path == "") {
      FileStatus status;
      auto commit = branch.commit();
      status.atime = commit.time();
      status.mtime = commit.time();
      status.type = file_type::symlink;
      status.mode = 0555;
      status.size = 40;
      return status;
    }

  } catch (git_error const& error) {
    throw filesystem_error("no branch directory: " + first_dir.string(), path, std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Branches::readdir(Directory const dir)
  -> vector<Path>
{
  if (dir == "" || dir == ".") {
    vector<Path> entries;
    for (auto const& branch : repository().branches())
      entries.push_back(branch.name());
    return entries;
  }
  throw filesystem_error("dir not known", dir, std::make_error_code(std::errc::not_a_directory));
}

auto GitFuse::Branches::readlink(Path path)
  -> Path
{
  auto const branch_id = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (remaining_path.empty()) {
    try {
      auto branch = repository().branch(branch_id);
      return Path("..") / "commits" / branch.commit().id();
    } catch (...) {
      throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_link));
    }
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}

auto GitFuse::Branches::read(File const path)
  -> string
{
  auto const branch_id = first_directory(path);
  auto const remaining_path = without_first_directory(path);
  if (remaining_path.empty()) {
    try {
      auto branch = repository().branch(branch_id);
      return branch.commit().id();
    } catch (...) {
      throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
    }
  }
  throw filesystem_error("path not known", path, std::make_error_code(std::errc::no_such_file_or_directory));
}
