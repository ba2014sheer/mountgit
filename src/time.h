#pragma once

#include <chrono>

class Year {
  public:
    explicit Year(int year);
    explicit Year(string year);
    operator int() const;
  private:
    int year_ = 0;
};
class Month {
  public:
    explicit Month(int month);
    explicit Month(string month);
    operator int() const;
  private:
    int month_ = 0;
};
class Day {
  public:
    explicit Day(int day);
    explicit Day(string day);
    operator int() const;
  private:
    int day_ = 0;
};

class Time : public std::chrono::system_clock::time_point {
  public:
    using Clock = std::chrono::system_clock;
  public:
    Time();
    Time(Clock::time_point time);
    Time(time_t time);
    Time(git_time time);

    operator time_t()            const;
    operator git_time()          const;

    auto year()  const -> Year;
    auto month() const -> Month;
    auto day()   const -> Day;
};
auto to_string(Time time)   -> string;

auto to_string(Year year)   -> string;
auto to_string(Month month) -> string;
auto to_string(Day day)     -> string;
