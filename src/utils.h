auto convert_into_argv(string program_name, vector<string> args) -> char**;
auto count_argv(char** argv) -> int;
void free_argv(char** argv);
