#pragma once

struct Options {
  bool with_child_reference = false; // deactivate for performance improvement
  bool with_childs_count    = true; // deactivate for performance improvement
  bool umask_with_w         = true; // write access to all elements, needed for using unionfs
};

extern Options options;
