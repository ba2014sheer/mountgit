#include "../constants.h"
#include "tree.h"
#include "tree_entry.h"
#include "commit.h"
#include "repository.h"

#include <git2.h>

namespace Git {
Tree::Tree(Commit const& commit) :
  repository_(commit.repository())
{
  auto error = git_commit_tree(const_cast<git_tree**>(&tree_), commit);
  if (error)
    throw *giterr_last();
}

Tree::Tree(TreeEntry const& entry) :
  repository_(entry.repository())
{
  auto error = git_tree_lookup(const_cast<git_tree**>(&tree_),
                               const_cast<git_repository*>(repository_.c_ptr()),
                               entry);
  if (error)
    throw *giterr_last();
}

Tree::Tree(Tree const& tree) :
  repository_(tree.repository())
{
  auto error = git_tree_dup(const_cast<git_tree**>(&tree_), const_cast<git_tree*>(tree.c_ptr()));
  if (error)
    throw *giterr_last();
}

Tree::Tree(Tree&& tree) :
  repository_(tree.repository_),
  tree_(tree.tree_)
{
  tree.tree_ = nullptr;
}

Tree::~Tree()
{
  git_tree_free(const_cast<git_tree*>(tree_));
}

auto Tree::c_ptr() const
-> git_tree const*
{
  return tree_;
}

Tree::operator git_tree const*() const
{
  return tree_;
}

auto Tree::repository() const
-> Repository const&
{
  return repository_;
}

auto Tree::size() const
-> size_t
{
  return git_tree_entrycount(tree_);
}

auto Tree::entries() const
-> vector<Entry>
{
  vector<Entry> entries;
  auto const n = size();
  entries.reserve(n);
  for (size_t i = 0; i < n; ++i) {
    entries.emplace_back(*this, git_tree_entry_byindex(tree_, i));
  }
  return entries;
}

auto Tree::entries(Directory const dir) const
-> vector<Entry>
{
  return Tree(entry(dir)).entries();
}

auto Tree::entry(Path const path) const
-> Entry
{
  return {*this, path};
}

} // namespace Git
