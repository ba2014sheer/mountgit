#pragma once

namespace Git {
class Branch;
class Commits;
class Commit;
class Tag;
}

struct git_repository;

namespace Git {
class Repository {
  public:
    explicit Repository(Directory path);
    Repository(git_repository* repository);
    Repository(Repository const&)                          = delete;
    Repository(Repository&& repository);
    auto operator=(Repository const&)       -> Repository& = delete;
    auto operator=(Repository&& repository) -> Repository&;
    ~Repository();

    auto c_ptr()       -> git_repository*;
    auto c_ptr() const -> git_repository const*;
    operator git_repository*();
    operator git_repository const*() const;

    auto workdir()           const -> string;
    auto branches()          const -> vector<Branch>;
    auto branch(string name) const -> Branch;
    auto commits()           const -> Commits;
    auto commit(string id)   const -> Commit;
    auto tags()              const -> vector<Tag>;
    auto tag(string name)    const -> Tag;

  private:
    git_repository* repository_ = nullptr;
    bool managed_ = false;
};
}
