#pragma once

#include "signature.h"
namespace Git {
class Repository;
}

struct git_commit;

namespace Git {
class Commit {
  public:
    using Id = string;
  public:
    Commit(Repository const& repository, string sha);
    Commit(Repository const& repository, git_oid const& oid);
    Commit(Commit const& commit);
    Commit(Commit&& commit);
    auto operator=(Commit const& commit) -> Commit& = delete;
    auto operator=(Commit&& commit)      -> Commit& = delete;
    ~Commit();

    auto c_ptr()     const -> git_commit const*;
    operator git_commit const*() const;

    auto repository() const -> Repository const&;
    auto id()         const -> Id;
    auto message()    const -> string;
    auto summary()    const -> string;
    auto body()       const -> string;
    auto time()       const -> Time;
    auto committer()  const -> Signature;
    auto author()     const -> Signature;
    auto parents()    const -> vector<Commit>;
    auto children()   const -> vector<Commit>;

  private:
    Repository const& repository_;
    mutable git_commit const* commit_ = nullptr;
};
}
