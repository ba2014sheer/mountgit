#pragma once

namespace Git {
class Repository;
class TreeEntry;
}

struct git_blob;

namespace Git {
class Blob {
  public:
    Blob(TreeEntry const& entry);
    Blob(Repository const& repository, git_oid const& oid);
    Blob(Blob const& blob)                    = delete;
    Blob(Blob&& blob);
    auto operator=(Blob const& blob) -> Blob& = delete;
    auto operator=(Blob&& blob)      -> Blob& = delete;
    ~Blob();

    auto c_ptr() const      -> git_blob const*;
    operator git_blob const*();

    auto repository() const -> Repository const&;
    auto id()         const -> string;

    auto content() const    -> string;
    auto size() const       -> size_t;

  private:
    Repository const& repository_;
    git_blob const* blob_ = nullptr;
};
}
