#pragma once

struct git_tag;
namespace Git {
class Repository;
class Signature;
}

namespace Git {
class Tag {
  public:
    Tag(Repository const& repository, string name);
    Tag(Tag const&)                    = delete;
    Tag(Tag&& tag);
    auto operator=(Tag const&) -> Tag& = delete;
    auto operator=(Tag&& tag)  -> Tag&;
    ~Tag();

    auto c_ptr() const -> git_tag const*;

    auto name() const -> string;
    auto message() const -> string;
    auto repository() const -> Repository;
    auto signature() const -> Signature;

  private:
    git_tag* tag_ = nullptr;
    git_commit* commit_ = nullptr;
    string name_;
};
}
