#pragma once

struct git_reference;
namespace Git {
class Repository;
class Commit;
}

namespace Git {
class Branch {
  public:
    Branch(Repository const& repository, string name);
    Branch(Branch const&)                      = delete;
    Branch(Branch&& branch);
    auto operator=(Branch const&)   -> Branch& = delete;
    auto operator=(Branch&& branch) -> Branch& = delete;
    ~Branch();

    auto c_ptr() const -> git_reference const*;
    operator git_reference const*() const;

    auto repository() const -> Repository const&;
    auto name() const       -> string;
    auto commit() const     -> Commit;

  private:
    Repository const& repository_;
    git_reference* branch_ = nullptr;
};
}
