#include "../constants.h"
#include "tree_entry.h"
#include "repository.h"
#include "blob.h"

#include <git2.h>

namespace Git {
TreeEntry::TreeEntry(Tree const& tree, Path const path) :
  repository_(tree.repository())
{
  auto error = git_tree_entry_bypath(const_cast<git_tree_entry**>(&entry_), tree, path.c_str());
  if (error)
    throw *giterr_last();
}

TreeEntry::TreeEntry(TreeEntry const& entry) :
  repository_(entry.repository_)
{
  auto error = git_tree_entry_dup(const_cast<git_tree_entry**>(&entry_), const_cast<git_tree_entry*>(entry.c_ptr()));
  if (error)
    throw *giterr_last();
}

TreeEntry::TreeEntry(Tree const& tree, git_tree_entry const* entry) :
  repository_(tree.repository())
{
  auto error = git_tree_entry_dup(const_cast<git_tree_entry**>(&entry_), entry);
  if (error)
    throw *giterr_last();
}

TreeEntry::TreeEntry(TreeEntry&& entry) :
  repository_(entry.repository_),
  entry_(entry.entry_)
{
  entry.entry_ = nullptr;
}

TreeEntry::~TreeEntry()
{
  git_tree_entry_free(const_cast<git_tree_entry*>(entry_));
}

auto TreeEntry::c_ptr() const
-> git_tree_entry const*
{
  return entry_;
}

TreeEntry::operator git_tree_entry const*() const
{
  return entry_;
}

TreeEntry::operator git_oid const*() const
{
  return git_tree_entry_id(entry_);
}

auto TreeEntry::repository() const
-> Repository const&
{
  return repository_;
}


auto TreeEntry::name() const
-> string
{
  return git_tree_entry_name(entry_);
}

auto TreeEntry::file_type() const
-> std::filesystem::file_type
{
  switch (git_tree_entry_filemode(entry_)) {
  case GIT_FILEMODE_UNREADABLE:
    throw filesystem_error("unknown file type", name(), std::make_error_code(std::errc::no_such_file_or_directory));
  case GIT_FILEMODE_TREE:
    return file_type::directory;
  case GIT_FILEMODE_BLOB:
  case GIT_FILEMODE_BLOB_EXECUTABLE:
    return file_type::regular;
  case GIT_FILEMODE_LINK:
    return file_type::symlink;
  case GIT_FILEMODE_COMMIT:
    throw filesystem_error("unknown file type", name(), std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("unknown file type", name(), std::make_error_code(std::errc::no_such_file_or_directory));
}

auto TreeEntry::stat() const
-> FileStatus
{
  switch (file_type()) {
  case file_type::directory: {
    FileStatus status;
    status.type = file_type::directory;
    status.mode = 0555;
    status.size = Tree(*this).size();
    return status;
  }
  case file_type::regular: {
    FileStatus status;
    status.type = file_type::regular;
    status.mode = (git_tree_entry_filemode(entry_) == GIT_FILEMODE_BLOB_EXECUTABLE
                   ? 0555
                   : 0444);
    status.size = Blob(*this).size();
    return status;
  }
  case file_type::symlink: {
    FileStatus status;
    status.type = file_type::symlink;
    status.mode = 0555;
    status.size = Blob(*this).size();
    return status;
  }
  default:
    throw filesystem_error("path not known", name(), std::make_error_code(std::errc::no_such_file_or_directory));
  }
  throw filesystem_error("path not known", name(), std::make_error_code(std::errc::no_such_file_or_directory));
}

auto
TreeEntry::content() const
-> string
{
  auto oid = git_tree_entry_id(entry_);
  switch (git_tree_entry_filemode(entry_)) {
  case GIT_FILEMODE_UNREADABLE:
    throw filesystem_error("path not known", name(), std::make_error_code(std::errc::no_such_file_or_directory));
  case GIT_FILEMODE_TREE:
    throw filesystem_error("path is a directory", name(), std::make_error_code(std::errc::no_such_file_or_directory));
    break;
  case GIT_FILEMODE_BLOB:
  case GIT_FILEMODE_BLOB_EXECUTABLE:
  case GIT_FILEMODE_LINK: {
    git_blob* blob = nullptr;
    auto error = git_blob_lookup(&blob, const_cast<git_repository*>(static_cast<git_repository const*>(repository_)), oid);
    if (error)
      throw *giterr_last();
    auto const content = string(static_cast<char const*>(git_blob_rawcontent(blob)), git_blob_rawsize(blob));
    git_blob_free(blob);
    return content;
  }
  case GIT_FILEMODE_COMMIT:
    throw filesystem_error("path not known", name(), std::make_error_code(std::errc::no_such_file_or_directory));
  }
  return {};
}

} // namespace Git
