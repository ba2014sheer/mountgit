#pragma once

#include "../constants.h"

struct git_signature;

namespace Git {
class Signature {
  public:
    Signature(git_signature const* signature);
    Signature(Signature const&)                         = delete;
    Signature(Signature&& signature)                    = delete;
    auto operator=(Signature const&)      -> Signature& = delete;
    auto operator=(Signature&& signature) -> Signature& = delete;

    auto name()  const -> string;
    auto email() const -> string;
    auto when()  const -> Time;

  private:
    git_signature const* signature_ = nullptr;
};

auto to_string(Signature signature) -> string;
}
