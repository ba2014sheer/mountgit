#pragma once

namespace Git {
class Repository;
class Commit;
class TreeEntry;
}

struct git_tree;

namespace Git {
class Tree {
  public:
    using Entry = TreeEntry;
  public:
    Tree(Commit const& commit);
    explicit Tree(TreeEntry const& entry);
    Tree(Tree const& tree);
    Tree(Tree&& tree);
    auto operator=(Tree const& tree) -> Tree& = delete;
    auto operator=(Tree&& tree)      -> Tree& = delete;
    ~Tree();

    auto c_ptr() const      -> git_tree const*;
    operator git_tree const*() const;

    auto repository() const -> Repository const&;

    auto size() const                 -> size_t;
    auto entries() const              -> vector<Entry>;
    auto entries(Directory dir) const -> vector<Entry>;
    auto entry(Path path) const       -> Entry;

  private:
    Repository const& repository_;
    mutable git_tree const* tree_ = nullptr;
};
}
