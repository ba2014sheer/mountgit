#pragma once

namespace Git {
class Repository;
class Commit;
}

namespace Git {
class Commits {
  public:
    explicit Commits(Repository const& repository);
    Commits(Commits const& commits);
    Commits(Commits&& commits);
    auto operator=(Commits const& commit) -> Commits& = delete;
    auto operator=(Commits&& commit)      -> Commits& = delete;
    ~Commits();

#ifdef TODO
    class const_iterator;
    auto begin() const -> const_iterator;
    auto end()   const -> const_iterator;
#endif

    operator vector<Commit>() const;

    auto operator()(string id)                       const -> Commit;

    auto operator()(Year year)                       const -> vector<Commit>;
    auto operator()(Year year, Month month)          const -> vector<Commit>;
    auto operator()(Year year, Month month, Day day) const -> vector<Commit>;

    auto count()                                const -> size_t;
    auto count(Year year)                       const -> size_t;
    auto count(Year year, Month month)          const -> size_t;
    auto count(Year year, Month month, Day day) const -> size_t;

    auto years()                        const -> vector<Year>;
    auto months(Year year)              const -> vector<Month>;
    auto days(Year year, Month month)   const -> vector<Day>;

  private:
    Repository const& repository_;
};
}
