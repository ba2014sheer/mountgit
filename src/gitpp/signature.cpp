#include "signature.h"

#include <git2.h>

namespace Git {
Signature::Signature(git_signature const* signature) :
  signature_(signature)
{ }

auto Signature::name() const
-> string
{
  return signature_->name;
}

auto Signature::email() const
-> string
{
  return signature_->email;
}

auto Signature::when() const
-> Time
{
  return signature_->when;
}

auto to_string(Signature const signature)
  -> string
{
  return signature.name() + " <" + signature.email() + ">";
}

} // namespace Git
