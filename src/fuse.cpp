#define FUSE_USE_VERSION 30
#include <fuse.h>

#include "constants.h"
#include "fuse.h"
#include "utils.h"

#include <cstring>
#include <unistd.h>

namespace {
Fuse* fuse_ptr = nullptr;
int getattr(char const* path, struct stat* st, fuse_file_info* fi);
int readdir(char const* path, void *buf, fuse_fill_dir_t filler,
            off_t offset, struct fuse_file_info* fi,
            enum fuse_readdir_flags flags);
int readlink(const char *, char *, size_t);
int read(char const* path, char *buffer, size_t size, off_t offset,
         struct fuse_file_info* fi);

static struct fuse_operations operations = {
  .getattr      = getattr,
  .readlink     = readlink,
  .read         = read,
  .readdir      = readdir,
};
}

Fuse::Fuse(Directory mountpoint, vector<string> options) :
  mountpoint_(mountpoint),
  options_(options)
{ }

Fuse::~Fuse()
{
}

auto Fuse::run()
  -> int
{
  auto options = options_;
  options.insert(options.begin(), mountpoint_);

  auto const argv = convert_into_argv("mountgit", options);
  auto const argc = options.size() + 1;

  int result = 0;
  try {
    assert(fuse_ptr == nullptr);
    fuse_ptr = this;
    result = fuse_main(argc, argv, &operations, NULL);
    assert(fuse_ptr == this);
    fuse_ptr = nullptr;
  } catch (git_error const& error) {
    CERR << error.message << endl;
    result = error.klass;
  } catch (...) {
    CERR << "unknown error" << endl;
  }

  free_argv(argv);

  return result;
}

auto Fuse::readlink(Path path)
  -> Path
{
  return {};
}


namespace {
int getattr(char const* path, struct stat* st, fuse_file_info* fi)
{
  try {
    copy(*st, fuse_ptr->getattr(path));
    if (options.umask_with_w)
      st->st_mode |= 0200;
    return 0;
  } catch (filesystem_error const& error) {
    //CERR << error.what() << ": " << error.path1() << '\n';
    return -error.code().value();
  }
}

int readdir(char const* path, void *buf, fuse_fill_dir_t filler,
            off_t offset, struct fuse_file_info* fi,
            enum fuse_readdir_flags flags)
{
  try {
    filler(buf, ".", nullptr, 0, FUSE_FILL_DIR_PLUS);
    filler(buf, "..", nullptr, 0, FUSE_FILL_DIR_PLUS);
    for (auto const entry : fuse_ptr->readdir(path))
      filler(buf, entry.c_str(), nullptr, 0, FUSE_FILL_DIR_PLUS);
    return 0;
  } catch (filesystem_error const& error) {
    CERR << error.what() << ": " << error.path1() << '\n';
    return -error.code().value();
  }
}

int readlink(char const* path, char* target_buffer, size_t buffer_size)
{
  try {
    auto const target = fuse_ptr->readlink(path).string();
    memcpy(target_buffer, target.c_str(), std::min(buffer_size, target.size() + 1));
    return 0;

  } catch (filesystem_error const& error) {
    CERR << error.what() << ": " << error.path1() << '\n';
    return -error.code().value();
  }
}

int read(char const* path, char *buffer, size_t size, off_t offset,
         struct fuse_file_info* fi)
{
  try {
    auto const text = fuse_ptr->read(path);
    if (text.empty())
      return -1;
    if (offset < 0)
      offset = 0;
    if (static_cast<string::size_type>(offset) >= text.length())
      return -1;
    size = min(size, text.size() - offset);
    memcpy(buffer, text.c_str() + offset, size);
    return size;
  } catch (filesystem_error const& error) {
    CERR << error.what() << ": " << error.path1() << '\n';
    return -error.code().value();
  }
}

}
